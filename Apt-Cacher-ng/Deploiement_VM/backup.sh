#!/bin/bash

date=$(date +%d-%m-%Y)
cd /var/lib/docker/volumes/apt-cacher-ng_apt-cacher-ng
cp -r _data/ /tmp/ 
cd /tmp 
tar zcvf backup-apt-cacher-$date.tar.gz _data
mount -t nfs 192.168.43.85:/tmp /mnt/data 
cp /tmp/backup-apt-cacher-$date.tar.gz /mnt/data
rm /tmp/backup-apt-cacher-$date.tar.gz && rm -rf /tmp/_data
umount /mnt/data

