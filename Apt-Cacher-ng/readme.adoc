= Installation d'un serveur APT-CACHER
:toc: left
:sectnums:



image:cacher.png[cacher]



== Installation du serveur

Dans un premier temps nous téléchargeons le paquet apt-cacher-ng avec la commande ci-dessous :

[source,bash]

$ apt-get install apt-cacher-ng

Le serveur est prêt et il est fonctionnel. Par défaut, il utilise les spécifité suivante : 

* *Fichiers de configuration* : /etc/apt-cacher-ng/acng.conf
** *Port* : 3142
** *CacheDir* : Répertoire où se trouveront les paquets en cache par défaut, ils se trouvent dans /var/cache/apt-cacher-ng
** *LogDir*: Répertoire où sont stockés les logs de Apt-cacher-ng par défaut ils se trouvent dans  : /var/log/apt-cacher-ng
** *BindAddress* : Sert à autoriser les addresses pouvant accéder à l'apt-cacher.

Afin de sécuriser l'accès aux tâches de maintenance, (ex : ). Nous devons modifier le fichier suivant  :

[source,bash]
 
 $ vim /etc/apt-cacher-ng/security.conf

et par exemple ajouter un mot de passe administrateur en ajoutant la ligne suivante : 


[source,bash]

 "AdminAuth: admin:elmotdepasse"

Puis redémarrer le service apt-cacher-ng en réalisant la commande suivante : 
[source,bash]
$ systemctl restart apt-cacher.ng


Nous pouvons ensuite aller vérifier celà en nous rendant sur l'addresse suivante : 

http://ip-du-serveur:3142

image:apt-cacher.png[web]



NOTE: Sur l’interface web de maintenance sert à donner une explication sur "l'import" vous y trouverez une courte explication pour l’import des paquets déjà dans le cache de votre serveur (pour Debian/Ubuntu c’est dans /var/cache/apt/archives), gérer l’expiration des paquets, des statistiques sur les transferts, etc.


== Installation sur les postes clients : 


Il suffit d'éditer le fichier suivant : 

[source,bash]
$ vim /etc/apt/apt.conf.d/70debconf

et y ajouter la ligne suivante :
[source,bash]

Acquire::http { Proxy "http://<ip du serveur>:<port>"; };


Vous pouvez maintenant faire un update.


== Et sous Docker ? 

Pour installer  Apt-cacher-ng nous allons utiliser comme pour le LDAP Docker Compose.

Nous écrivons donc notre fichier YAML de la manière suivante : 

[source,yaml]

----
version: '3'

services:
  apt-cacher-ng:
    image: sameersbn/apt-cacher-ng
    container_name: apt-cacher-ng
    ports:
      - "3142:3142"
    volumes:
      - apt-cacher-ng:/var/cache/apt-cacher-ng
    restart: always

volumes:
  apt-cacher-ng:
----


Description du fichier YAML : 

[source,yaml]

----
version: '3'

services:
  apt-cacher-ng:                    -> Le nom du service
    image: sameersbn/apt-cacher-ng  -> L'image que l'on veut utiliser
    container_name: apt-cacher-ng   -> Le nom du conteneur 
    ports:
      - "3142:3142"                 -> Le port exposé
    volumes:
      - apt-cacher-ng:/var/cache/apt-cacher-ng   -> Le volume associé au conteneur 
    restart: always

volumes:
  apt-cacher-ng:
----

Nous faisons simplement un :  

[source,bash]

$ docker-compose up - d

Sur les clients nous créons le fichier 01proxy comme vu ci-dessus :

[source,bash]

Acquire::HTTP::Proxy "http://IPDUPROXY:3142";
Acquire::HTTPS::Proxy "false";


== Best practices


- Il faut ouvrir les ports *TCP 3142 sortant* pour les postes clients et le port *TCP 3142 entrant* pour le serveur.

- Pour consulter les logs dans le conteneur Apt-cacher, il faudra taper la commande suivante : 

[source,bash]
docker exec -it apt-cacher-ng tail -f /var/log/apt-cacher-ng/apt-cacher.log
