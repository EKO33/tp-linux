#!/bin/bash

if [ -e /usr/bin/vagrant ]; then 
    echo -e "\E[32m Vagrant est bien installé \E[0m"
else 
    echo -e "\E[32m Veuillez installer Vagrant \E[0m"
    exit
fi

if [ -e /usr/bin/virtualbox ]; then 
    echo -e "\E[32m Virtualbox est bien installé \E[0m"
else 
    echo -e "\E[32m Veuillez installer Virtualbox \E[0m" 
    exit
fi 

if [ -e /etc/ansible ]; then 
    echo -e "\E[32m Ansible est bien installé \E[0m"
else 
    echo -e "\E[32m Veuillez installer Ansible \E[0m"
    exit
fi

REPON=""

while [ "$REPON" != "Q" ] ; do
    REPON=""

echo -e "\n#################################################"
echo "Procédure d'installation des serveurs d'infra.lab"
echo "################################################# "

        echo " "
        echo "    Installer :"
        echo " "
        echo "    1 - Supervision"
        echo "    2 - Apt-Cacher"
        echo "    3 - Backup"
        echo "    4 - DHCP"
        echo "    5 - PXE"
        echo "    6 - LDAP"
        echo "    7 - DNS PRIMAIRE"
        echo "    8 - DNS SECONDAIRE"
        echo "    9 - WEB"
        echo " "
        echo 
        echo -n  "    Choix (1, 2, ... ou q) ? "
        read REPON
      
        REPON="`echo $REPON | tr '[a-z]' '[A-Z]'`"

    case "$REPON" in
        1) 
		        ;;
        2) cd Apt-Cacher-ng/Deploiement_VM && vagrant up
		        ;;
        3) cd BACKUPS/Deploiement_vm && vagrant up
		        ;;
        4) cd DHCP/Deploiement_vm && vagrant up 
		        ;;
        5) cd PXE/Deploiement_vm && vagrant up
		        ;;
        6) cd LDAP/Deploiement_VM && vagrant up
		        ;;
        7) cd Dns/Deploiement_VM/SRV-DNS1 && vagrant up
		;;
        8) cd Dns/Deploiement_VM/SRV-DNS2 && vagrant up
		;;
        9) cd Wordpress/Deploiement_VM/ && vagrant up
		;;

        Q) break
        ;;
        *) echo -e "\nRéponse non valide." >&2
        ;;
    esac
done

